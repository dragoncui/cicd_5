# 厂商新增openharmony门禁指导（注意：三方开发板的门禁周期为1个月上线门禁工程，1个月后则移到每日构建。）

步骤1：厂商找王少峰（OH南向设备厂商接口人，邮箱：wangshaofeng5@huawei.com）申请上线流水线。\
步骤2：开发厂商修改配置文件：\
(1)manifest仓default.xml文件，在OH分支上链入新设备仓。\
(2)修改精准构建文件
https://gitee.com/openharmony/manifest/blob/master/matrix_product.csv

      注意：
      1. 如果default.xml 新加的单个仓 大小超过 100M，需要通知OH CMO 马明帅 （邮箱：mamingshuai1@huawei.com），对新链入仓 进行预热，避免上线后，后台流水线编译服务器同时下载新仓，带宽不够，导致流水线大规模下载失败。
      2.之前南向设备上门禁纪要对齐：后续新设备上线， matrix_product.csv只监控设备仓的提交构建。 公共仓不监控。修改后提交PR请 李兵 （邮箱：libing3@huawei.com） 审查合入，厂商精准构建文件由李兵看护。

步骤3：王少峰上研发效率例会评审，通过后通知胡敏安排人实施。


<font color="red">
注意：编译命令中不要添加下载编译工具等命令，如有需要，联系王意明（邮箱：youthdragon.wangyiming@huawei.com）将工具集成到docker中，编译只构建产物！！
</font>


##### 步骤1 新加设备门禁形态模板

    申请名称: 小熊派开发板门禁项目
    申请单位：南京小熊派智能科技有限公司
    工程名称：openharmony
    manifest分支：master
    申请背景：小熊派BearPi_HM_Micro开发板【STM32MP157】适配openharmony
    代码分支名：master
    manifest仓配置文件：default.xml
    编译形态名（必填）: BearPi_HM_Micro （注意不要带空格）
    预编译命令: 无
    编译命令（必填）:python3 build.py -p bearpi_hm_micro@bearpi --tee -f     
    后编译命令：无
    产物路径（必填）: out/bearpi_hm_micro/bearpi_hm_micro/OHOS_Image.stm32
    out/bearpi_hm_micro/bearpi_hm_micro/userfs_vfat.img
    out/bearpi_hm_micro/bearpi_hm_micro/rootfs_vfat.img

##### 步骤2 csv文件填写方法

1.	找到对应工程的manifest仓，点击【克隆/下载】，将仓库文件下载到本地
2.	解压并打开matrix_product.csv文件，组成如下
 ![csv填写示例](http://image.huawei.com/tiny-lts/v1/images/c694f45f99356349045e6d143ba98525_1053x398.png)
3.	增加仓库，需要添加一行，同时修改default.xml文件；增加设备形态，需要增加一列，并将这一列补充完整。
4.	填写完成后保存，用notepad++打开，全选复制，替换到码云中，并对比差异
5.	检查无误后，在提交信息中填入签名信息: Signed-off-by: 【码云名称】<【邮箱】>，并选择【提交并创建轻量级PR】，点击提交，私仓合入后合入主仓
6.	评论 start build 触发门禁，待编译测试都通过后，找 李兵（邮箱：libing3@huawei.com） 审查合入。
7.	Default.xml文件的修改方法及详情，查看manifest仓下README.md\
Manifest仓：https://gitee.com/openharmony/manifest

    参考样例:
    加仓：https://gitee.com/openharmony/manifest/pulls/588/files
    加编译形态：https://gitee.com/openharmony/manifest/pulls/586/files
